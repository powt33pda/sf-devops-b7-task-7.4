terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = ""
  cloud_id  = "cloud-33pda"
  folder_id = "b1gk2ai7nhqsar0e9k55"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "sf-b7-vm01" {
  name        = "sf-b7-vm01"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "sf-b7-vm01"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd80viupr3qjr5g6g9du"
      size = 50
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b7-4.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/sf_b7_vm01.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_compute_instance" "sf-b7-vm02" {
  name        = "sf-b7-vm02"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "sf-b7-vm02"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd80viupr3qjr5g6g9du"
      size = 50
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b7-4.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/sf_b7_vm02.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_vpc_network" "sf-b7-4" { }
  
resource "yandex_vpc_subnet" "sf-b7-4" {
    zone       = "ru-central1-a"
    network_id = "${yandex_vpc_network.sf-b7-4.id}"
    v4_cidr_blocks = ["172.16.0.0/16"]
}