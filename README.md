# Введение
Репозиторий для выполнения задания B7.4 курса Skill Factory DevOps.

# Гайд
Гайд доступен по ссылке: https://graceful-hydrant-589.notion.site/B7-4-b73228efe71e4e0ca69848da354fc74a

# Структура
 - cni - конфиги cni провайдеров
 - keys - ключи для доступа на хосты
 - kubernetes - скрипты для установки и настройки Kubernetes
 - yandex - терраформ файлы для развертывания окружения в облаке Yandex Cloud